namespace va {

    class list {
        public:
            template <typename T>
            list(T &v);

            void end();
    
            template <typename T>
            T arg();

            operator char *();
            
        private:
            char *m_list;
    };

    /**
     * @brief Rounds the size of T to the nearest 32 bits
     */
    template <typename T>
    struct rounded_size {
        static const long value = ((sizeof(T) + sizeof(int) - 1) 
                                    / sizeof(int)) * sizeof(int);
    };

    /**
     * @brief Create a va::list.
     * @details Produces undefined behavior if passed an invalid pointer.
     * @param v The final argument passed to the function on the stack.
     */
    template <typename T>
    list::list(T &v) : m_list((char *)(&v) + rounded_size<T>::value) { }

    /**
     * @brief Gets the next variadic argument in the va::list.
     * @details Produces undefined behavior if used with the wrong type, or
     * on more arguments than were passed to the function.
     * @param ap The va::list to extract an argument from.
     */
    template <typename T>
    T list::arg() {
        m_list += rounded_size<T>::value;
        return (*((T*)(m_list - rounded_size<T>::value)));
    }

    /**
     * @brief Terminate a va::list.
     * @details Should always be called to ensure a va::list is not used after
     * all arguments have been extracted.
     * @param ap The va::list to terminate.
     */
    void list::end() {
        m_list = 0;
    }

    /**
     * @brief Convert a va::list to a `char *'.
     * @details For backwards compatibility with `va_list'.
     */
    list::operator char *() {
        return m_list;
    }
}

